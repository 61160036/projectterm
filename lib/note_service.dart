import 'note.dart';
import 'package:intl/intl.dart';

var lastid = 3;
var mockNotes = [
  Note(id: 1, name: 'Love', text: 'werwerwersdfsdf', date: '2021-10-29 12:30')
];

String getDate() {
  DateTime now = DateTime.now();
  String formattedDate = DateFormat('yyyy-MM-dd kk:mm').format(now);
  return formattedDate;
}

int getNewId() {
  return lastid;
}

Future<void> addNewNote(Note note) {
  return Future.delayed(Duration(seconds: 1), () {
    mockNotes.add(Note(
        id: getNewId(), name: note.name, text: note.text, date: note.date));
  });
}

Future<void> EditNote(Note note) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockNotes.indexWhere((item) => item.id == note.id);
    mockNotes[index] = note;
  });
}

Future<void> DeleteNote(Note note) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockNotes.indexWhere((item) => item.id == note.id);
    mockNotes.removeAt(index);
  });
}

Future<List<Note>> getNotes() {
  return Future.delayed(Duration(seconds: 1), () => mockNotes);
}
