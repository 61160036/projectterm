import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'note.dart';
import 'note_service.dart';

class NoteForm extends StatefulWidget {
  Note note;
  NoteForm({Key? key, required this.note}) : super(key: key);

  @override
  _NoteFormState createState() => _NoteFormState(note);
}

class _NoteFormState extends State<NoteForm> {
  final _formKey = GlobalKey<FormState>();
  Note note;
  _NoteFormState(this.note);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.amber[100],
        appBar: AppBar(title: Text('Note')),
        body: Form(
          key: _formKey,
          child: Container(
            padding: EdgeInsets.all(15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextFormField(
                  autofocus: true,
                  initialValue: note.name,
                  decoration: InputDecoration(labelText: 'Name'),
                  onChanged: (String? value) {
                    note.name = value!;
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please input Name';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  initialValue: note.text,
                  decoration: InputDecoration(labelText: 'Text'),
                  onChanged: (String? value) {
                    note.text = value!;
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please input Text';
                    }
                    return null;
                  },
                ),
                ElevatedButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        if (note.id > 0) {
                          await EditNote(note);
                        } else {
                          await addNewNote(note);
                        }
                        Navigator.pop(context);
                      }
                    },
                    child: Text('Confirm'))
              ],
            ),
          ),
        ));
  }
}
