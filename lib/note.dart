import 'package:flutter/cupertino.dart';

class Note {
  int id;
  String name;
  String text;
  String date;

  Note({
    required this.id,
    required this.name,
    required this.text,
    required this.date,
  });

  Map<String, dynamic> toMap() {
    return {
      'No': id,
      'Name': name,
      'Text': text,
      'Date': date,
    };
  }

  static List<Note> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return Note(
          id: maps[i]['No'],
          name: maps[i]['Name'],
          text: maps[i]['Text'],
          date: maps[i]['Date']);
    });
  }

  String toString() {
    return 'Note {No: $id, Name: $name , Text: $text}, Date: $date';
  }
}
