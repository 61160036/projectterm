import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'note.dart';
import 'note_form_widget.dart';
import 'note_service.dart';

class Noteapp extends StatefulWidget {
  Noteapp({Key? key}) : super(key: key);

  @override
  _NoteappState createState() => _NoteappState();
}

class _NoteappState extends State<Noteapp> {
  late Future<List<Note>> _notes;
  @override
  void initState() {
    super.initState();
    _notes = getNotes();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text('Notes'),
        actions: [
          IconButton(
            onPressed: () async {
              Note newNote = Note(id: -1, name: '', text: '', date: '');
              await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => NoteForm(note: newNote)));
              setState(() {
                _notes = getNotes();
              });
            },
            icon: Icon(Icons.add_box_rounded),
            padding: EdgeInsets.all(25),
          ),
        ],
      ),
      body: FutureBuilder(
        future: _notes,
        builder: (context, snapshot) {
          List<Note> notes = snapshot.data as List<Note>;
          if (snapshot.hasError) {
            return Text('Error');
          }
          return ListView.builder(
            itemBuilder: (context, index) {
              var note = notes.elementAt(index);
              return ListTile(
                title: Card(
                  clipBehavior: Clip.antiAlias,
                  child: Column(
                    children: [
                      ListTile(
                        title: Text(
                          '${note.name}',
                          style: TextStyle(fontSize: 30.0, color: Colors.red),
                        ),
                        subtitle: Text('${note.date}'),
                        trailing: IconButton(
                          icon: Icon(
                            Icons.delete,
                            color: Colors.red[500],
                          ),
                          onPressed: () async {
                            await DeleteNote(note);
                            setState(() {
                              _notes = getNotes();
                            });
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(
                          '${note.text}',
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                    ],
                  ),
                ),
                onTap: () async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => NoteForm(note: note)));
                  setState(() {
                    _notes = getNotes();
                  });
                },
              );
            },
            itemCount: notes.length,
          );
        },
      ),
    );
  }
}
